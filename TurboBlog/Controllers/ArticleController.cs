﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TurboBlog.Database;
using TurboBlog.Models;

namespace TurboBlog.Controllers
{
    public class ArticleController : Controller
    {
        private readonly IGeneralContext _context;

        public ArticleController(IGeneralContext context)
        {
            _context = context;
        }

        public IActionResult Create()
        {
            return View();
        }
        public IActionResult Submit()
        {
            var form = HttpContext.Request.Form;
            _context.Articles.Add(new Article(form["title"], form["article"]));
            _context.UpdateChanges();
            return View();
        }

        public IActionResult CurrentArticle(int articleId)
        {
            return View(_context.Articles.ToArray()[articleId]);
            //return View(context.Articles.First(art => art.Id == articleId));
        }
    }
}
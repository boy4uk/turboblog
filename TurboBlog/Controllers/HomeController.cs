﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using TurboBlog.Database;
using TurboBlog.Models;

namespace TurboBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGeneralContext _context;

        public HomeController(IGeneralContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View(_context);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult UserLogged()
        {
            var form = HttpContext.Request.Form;
            var login = form["login"];
            var password = form["password"];
            Response.Cookies.Append("login", login);
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private bool Contains(string login)
        {
            return true;
        }
        private bool Check(string login, string password)
        {
            return true;
        }
    }
}

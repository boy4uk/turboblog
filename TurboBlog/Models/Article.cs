﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TurboBlog.Models
{
    public class Article
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }

        public DateTime CreationTime { get; set; }

        public Article()
        {
        }

        public Article(string title, string content)
        {
            Title = title;
            Content = content;
            CreationTime = DateTime.Now;
        }
    }
}

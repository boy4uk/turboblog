﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurboBlog.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public Author()
        {
        }

        public Author(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}

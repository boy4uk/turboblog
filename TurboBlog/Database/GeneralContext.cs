﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TurboBlog.Models;

namespace TurboBlog.Database
{
    public class GeneralContext : DbContext, IGeneralContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseNpgsql(
                "Host=ec2-79-125-4-96.eu-west-1.compute.amazonaws.com;" +
                "Database=d8k1qio3m0kvpr;" +
                "Username=rounuearajjuxd;" +
                "Port=5432;" +
                "Password=385a4372e725646c91f802da99282963a46fcee4d7dd9c00fd5f1f53c334e150;" +
                "SslMode=Require;Trust Server Certificate=True;");
        }

        public void UpdateChanges()
        {
            SaveChanges();
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}

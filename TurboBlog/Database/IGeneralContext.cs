﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TurboBlog.Models;

namespace TurboBlog.Database
{
    public interface IGeneralContext
    {
        DbSet<Article> Articles { get; set; }
        DbSet<Author> Authors { get; set; }

        void UpdateChanges();
    }
}
